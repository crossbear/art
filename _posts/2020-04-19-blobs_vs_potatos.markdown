---
layout: post
title:  "Blobs VS Potatos"
date:   2020-02-23 19:49:37 -0300
categories: jekyll update
---



Blobs are miscellaneous creatures of the day. They are made of random splotches of liquid and a mysterious scientific material called flubber. 

Here's a picture of one, but only special glasses can see them. 

[The first blob](/art/assets/20200418_182301000_iOS.jpg)

Blobs main food/only food that they eat is potatoes. They eat everything from tatertots, mashedpotatos, french-fries, and even potatoes raw. One day 
a mad scientist named, Albert Blobstien, created a serum that could make any inanimate object ALIVE! One day while the scientist was eating raw
potatoes, a drop of the serum poured onto one of the potatoes. Soon the potatoes grew arms and legs and started to gain common sense. When it realized
the blobs were eating its brothers and sisters, it went to action. At first the blobs didn't realize their food was rebelling against them. Soon the 
potatoes rised in numbers, quickly growing and making more serum. One day the potato that started the Rebelion, captured a blob. Soon the blobs 
realized that their food was starting a Rebelion, a "Civil War" if you will. The blobs never thought that they would have to do this, but they felt the
need to start to make a military with weapons. Because blobs couldn't die and the just morph into more blobs. This was a start of something big. 
Something that would last for over a year. This was the Blobs vs. Potatoes. Watch out, for the blobs will eat every last potato in your household... 

 